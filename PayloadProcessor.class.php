<?php
class PayloadProcessor
{
	static function get_A2S_INFO(ServerQuery $query)
	{
		$payload = $query->query(ServerQuery::QUERY_A2S_INFO);

		if($payload === false)
		{
			return false;
		}

		$buffer = new BufferProcessor($payload);

		switch($query->getEngine())
		{
			case Engine::GOLDSOURCE:
				return self::unpack_A2S_INFO_Goldsource($buffer);
				break;

			case Engine::SOURCE:
				return self::unpack_A2S_INFO_Source($buffer);
				break;

			default:
				throw new Exception('Unknown engine: '.$engine);
		}
	}

	static function unpack_A2S_INFO_Goldsource(BufferProcessor $buffer)
	{
		$data['Header'] = $buffer->extractByte();
		$data['Address'] = $buffer->extractString();
		$data['Name'] = $buffer->extractString();
		$data['Map'] = $buffer->extractString();
		$data['Folder'] = $buffer->extractString();
		$data['Game'] = $buffer->extractString();
		$data['Players'] = ord($buffer->extractByte());
		$data['MaxPlayers'] = ord($buffer->extractByte());
		$data['Protocol'] = ord($buffer->extractByte());
		$data['ServerType'] = $buffer->extractByte();
		$data['Environment'] = $buffer->extractByte();
		$data['Visibility'] = ord($buffer->extractByte());
		$data['Mod'] = ord($buffer->extractByte());

		if($data['Mod'] == 1)
		{
			$data['Link'] = $buffer->extractString();
			$data['DownloadLink'] = $buffer->extractString();
			// NULL byte
			$buffer->extractByte();
			$data['Version'] = $buffer->extractLong();
			$data['Size'] = $buffer->extractLong();
			$data['Type'] = ord($buffer->extractByte());
			$data['DLL'] = ord($buffer->extractByte());
		}

		$data['VAC'] = ord($buffer->extractByte());
		$data['Bots'] = ord($buffer->extractByte());

		return $data;
	}

	static function unpack_A2S_INFO_Source(BufferProcessor $buffer)
	{
		$data['Header'] = $buffer->extractByte();
		$data['Protocol'] = ord($buffer->extractByte());
		$data['Name'] = $buffer->extractString();
		$data['Map'] = $buffer->extractString();
		$data['Folder'] = $buffer->extractString();
		$data['Game'] = $buffer->extractString();
		$data['ID'] = $buffer->extractShort();
		$data['Players'] = ord($buffer->extractByte());
		$data['MaxPlayers'] = ord($buffer->extractByte());
		$data['Bots'] = ord($buffer->extractByte());
		$data['ServerType'] = $buffer->extractByte();
		$data['Environment'] = $buffer->extractByte();
		$data['Visibility'] = ord($buffer->extractByte());
		$data['VAC'] = ord($buffer->extractByte());

		// The Ship
		if($data['ID'] == 2400)
		{
			$data['Mode'] = $buffer->extractByte();
			$data['Witnesses'] = $buffer->extractByte();
			$data['Duration'] = $buffer->extractByte();
		}

		$data['Version'] = $buffer->extractString();
		$data['EDF'] = $buffer->extractByte();

		if($data['EDF'] & 0x80)
		{
			$data['Port'] = $buffer->extractShort();
		}

		if($data['EDF'] & 0x10)
		{
			$data['SteamID'] = $buffer->extractLongLong();
		}

		if($data['EDF'] & 0x40)
		{
			$data['SourceTVPort'] = $buffer->extractShort();
			$data['SourceTVName'] = $buffer->extractString();
		}

		if($data['EDF'] & 0x20)
		{
			$data['Keywords'] = $buffer->extractString();
		}

		if($data['EDF'] & 0x01)
		{
			$data['GameID'] = $buffer->extractLongLong();
		}

		return $data;
	}
}