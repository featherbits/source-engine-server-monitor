<?php
class BufferProcessor
{
	private $buffer;
	private $pointer = 0;

	function __construct($buffer)
	{
		$this->buffer = $buffer;
	}

	function getBuffer()
	{
		return substr($this->buffer, $this->pointer);
	}

	function reset($buffer)
	{
		$this->buffer = $buffer;
		$this->pointer = 0;
	}

	function splitByte($value)
	{
		$value = sprintf('%08d', decbin($value));
		$result[] = substr($value, 0, 4);
		$result[] = substr($value, 5, 8);

		return $result;
	}

	function getMsbLong($value)
	{
		$value = sprintf('%032d', decbin($value));
		return substr($value, 0, 1);
	}

	function readByte()
	{
		return substr($this->buffer, $this->pointer, 1);
	}

	/**
	 * Extract 1 byte from buffer
	 *
	 * @return string
	 */
	function extractByte()
	{
		$byte = $this->readByte();
		$this->pointer += 1;

		return $byte;
	}

	/**
	 * Extract 2 bytes from buffer
	 *
	 * @return int
	 */
	function extractShort()
	{
		$short = substr($this->buffer, $this->pointer, 2);
		$val = unpack('v', $short);
		$this->pointer += 2;

		return reset($val);
	}

	function readLong()
	{
		$long = substr($this->buffer, $this->pointer, 4);
		$val = unpack('V', $long);

		return reset($val);
	}

	/**
	 * Extract 4 bytes from buffer
	 *
	 * @return int
	 */
	function extractLong()
	{
		$long = $this->readLong();
		$this->pointer += 4;

		return $long;
	}

	/**
	 * Extract 4 bytes from buffer
	 *
	 * @return int
	 */
	function extractLongLong()
	{
		$long = $long = substr($this->buffer, $this->pointer, 8);
		$this->pointer += 8;
		$val = unpack('V2', $packet, $packet >> 32);

		return reset($val);
	}

	/**
	 * Extract all bytes from buffer till 0x00 terminator occurs
	 *
	 * @return string
	 */
	function extractString()
	{
		$strend = strpos($this->buffer, "\x00", $this->pointer);
		$string = substr($this->buffer, $this->pointer, ($strend - $this->pointer));
		$this->pointer = $strend + 1;

		return $string;
	}
}
