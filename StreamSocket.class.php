<?php
class StreamSocket
{
	private $resource;

	/**
	 * Creates a connection to remote socket
	 */
	function __construct($ip, $port, $timeout)
	{
		$this->resource = stream_socket_client('udp://'.$ip.':'.$port, $errno, $errstr);
		
		if(!$this->resource)
		{
			throw new Exception("Connection error: [$errno] $errstr");
		}

		stream_set_timeout($this->resource, $timeout);
	}

	function write($str)
	{
		fputs($this->resource, $str);
	}

	function read($length = 1400)
	{
		return fread($this->resource, $length);
	}

	function close()
	{
		fclose($this->resource);
	}

	function __destruct()
	{
		$this->close();
	}
}