<?php
class ServerQuery
{
	/**
	 * Query
	 * Basic information about the server.
	 */
	const QUERY_A2S_INFO = "\xFF\xFF\xFF\xFF\x54\x53\x6F\x75\x72\x63\x65\x20\x45\x6E\x67\x69";

	private $stream;
	private $engine;

	function __construct(StreamSocket $stream)
	{
		$this->stream = $stream;
	}

	/**
	 * Query the server and get payload
	 *
	 * @param string query
	 * @param string server engine
	 * @return array payload
	 */
	function query($q)
	{
		$this->stream->write($q);
		$response = $this->stream->read();

		if(empty($response))
		{
			return false;
		}

		$buffer = new BufferProcessor($response);

		// Multi-packet Response
		if($buffer->readLong() == -2)
		{
			// Try to detect engine by simulating steps to Goldsource Server Payload header
			$_header = $this->simulateGetGoldsourcePayloadHeader($response);
			$this->detectEngine($_header, 'I');

			switch($this->engine)
			{
				case Engine::GOLDSOURCE:
					return $this->multiPacketResponse_GoldSrc($buffer);
					break;

				case Engine::SOURCE:
					return $this->multiPacketResponse_Source($buffer);
					break;

				default:
					throw new Exception('Unknown engine: '.$engine);
			}
		}

		// Simple Response
		$buffer->extractLong();
		$this->detectEngine($buffer->readByte());
		return $buffer->getBuffer();
	}

	private function simulateGetGoldsourcePayloadHeader($response)
	{
		$_tmp_buffer = new BufferProcessor($response);
		$_tmp_buffer->extractLong();
		$_tmp_buffer->extractLong();
		$_tmp_buffer->extractByte();
		return $_tmp_buffer->extractByte();
	}

	private function detectEngine($payloadHeader, $defaultHeader = false)
	{
		switch($payloadHeader)
		{
			case 'I':
				$this->engine = Engine::SOURCE;
				break;

			case 'm':
				$this->engine = Engine::GOLDSOURCE;
				break;

			default:
				if($defaultHeader)
				{
					$this->detectEngine($defaultHeader);
				}
				else
				{
					throw new Exception('Unknown engine: '.$engine);
				}
				break;
		}
	}

	function getEngine()
	{
		return $this->engine;
	}

	/**
	 * Gets Multi-packet Response for GoldSource engine servers
	 *
	 * @param BufferProcessor $buffer
	 * @return string payload
	 */
	private function multiPacketResponse_GoldSrc(BufferProcessor $buffer)
	{
		while(true)
		{
			// Remove header
			$buffer->extractLong();

			$id = $buffer->extractLong();
			$packetNumber = $buffer->splitByte($buffer->extractByte());
			$currentPacket = $packetNumber[0];
			$packetCount = $packetNumber[1];

			$payload[$currentPacket] = $buffer->getBuffer();

			if((count($payload) == $packetCount))
			{
				break;
			}

			// Buffer for next packet
			$buffer->reset($this->stream->read());
		}

		ksort($payload);
		return implode($payload);
	}

	/**
	 * Gets Multi-packet Response for Source engine servers
	 *
	 * @param BufferProcessor $buffer
	 * @return string
	 */
	private function multiPacketResponse_Source(BufferProcessor $buffer)
	{
		$isCompressed = false;

		while(true)
		{
			// Remove header
			$buffer->extractLong();

			$id = $buffer->extractLong();

			// Most significant bit indicates compresed response
			if($buffer->getMsbLong($id) == 1)
			{
				$isCompressed = true;
			}

			$packetCount = $buffer->extractByte();
			$currentPacket = $buffer->extractByte();

			if($isCompressed and $currentPacket == 0)
			{
				$buffer->extractLong();
				$checksum = $buffer->extractLong();
			}
			// Maximum size of packet before packet switching occurs.
			else if(!$isCompressed)
			{
				// If this field is not present then next byte would be Header from Payload
				if($buffer->readByte() != 'I')
				{
					$buffer->extractShort();
				}
			}

			$payload[$currentPacket] = $buffer->getBuffer();

			if((count($payload) == $packetCount))
			{
				break;
			}

			// Buffer for next packet
			$buffer->reset($this->stream->read());
		}

		ksort($payload);
		$concatenatedPayload = implode($payload);

		if($isCompressed)
		{
			if(function_exists('bzdecompress'))
			{
				$decompresedPayload = bzdecompress($concatenatedPayload);

				if(sprintf("%u", crc32($decompresedPayload)) == $checksum)
				{
					return $decompresedPayload;
				}
				else
				{
					throw new Exception('Decompresed payload checksum mismatch');
				}
			}
			else
			{
				throw new Exception('Function bzdecompress does not exist');
			}
		}

		return $concatenatePayload;
	}
}
